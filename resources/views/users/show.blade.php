@extends('layouts.app')

@section('title', 'User')

@section('content')
  
    @if(Session::has('notallowed'))
        <div class = 'alert alert-danger'>
            {{Session::get('notallowed')}}
        </div>
    @endif
    <h1>User details</h1>
    <table class = "table table-dark">
        <tr>
            <td>Id</td>
            <td>{{$user->id}}</td>
        </tr>
        <tr>
            <td>Name</td>
            <td>{{$user->name}}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>{{$user->email}}</td>
        </tr> 

        <tr>
            <td>Department Id</td>
            <td>{{$user->department_id}}</td>
        </tr> 
     
        <tr>
            <td>Created</td>
            <td>{{$user->created_at}}</td>
        </tr>
        <tr>
            <td>Updated</td>
            <td>{{$user->updated_at}}</td>  
        </tr>    
        @if(Gate::allows('change-dep'))
            <tr>
            <td>currnet department</td>
            <td>{{$user->department->name}}</td>
            </tr> 
            <td>
            
        
            <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                       
                          change dep
                        
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach($departments as $department)
                      <a class="dropdown-item" href="{{route('changeuser',[$user->id,$department->id])}}">{{$department->name}}</a>
                    @endforeach
                    </div>
                  </div>
            </td>

            

            @else
            <tr>
            <td>currnet department</td>
            <td>{{$user->department->name}}</td>
            </tr>
            @endif



    </table>
@endsection
