@extends('layouts.app')

@section('title', 'Users')

@section('content')
<!-- @if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif -->
<h1>List of users</h1>
<table class = "table table-dark">
    <tr>
        <th>ID</th><th>Department</th>
    </tr>
    <!-- the table data -->
    @foreach($users as $user)
        <tr>
            <td>{{$user->name}}</td>
            <td>{{$user->department->name}}</td>
                       <td>
                    <a href = "{{route('user.delete',$user->id)}}">Delete</a>
            </td> 
            
              
            <td>
                    <a href = "{{route('user.show',$user->id)}}">Show Details</a>
            </td>  



                                                                
        </tr>
    @endforeach
</table>
@endsection

